import random
from ivoex.program import *

class search_session():
    id = None
    search_name = ""
    podcast_list = []
    current_podcast = None
    current_episode = None
    mpd_player_settings = None
    current_buffer = None
    
    episode_page = 1
    
    def __init__(self):
        self.id = int.from_bytes(bytes([random.randrange(255) for _ in range(4) ]),"big",signed=False)
        
    def search_programs(self,search_name):
        self.podcast_list = search_podcast(search_name)
        self.search_name = search_name
        self.podcast_list_page = 1
        
    def search_episodes(self):
        self.current_podcast.get_episodes()        

def get_current_session(req,global_dict):
    sid = req.args.get("id")
    current_session = None
    exists = False    

    if sid != None and sid != "":
        if int(sid) in global_dict:
            current_session = global_dict[int(sid)]
            exists = True
        else:
            #TODO: advise of internal error
            current_session = search_session()
            global_dict[int(current_session.id)] = current_session
    else:
        current_session = search_session()
        global_dict[int(current_session.id)] = current_session
            
    return current_session,exists
