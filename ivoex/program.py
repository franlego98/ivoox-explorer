from ivoex.episode import *
import requests
import AdvancedHTMLParser

class podcast:
    name = None
    url = None
    num_pages = None
    img_url = None
    num_progs = None
    producer = None
    episodes = []
    
    def __init__(self,url,name=None,img_url=None,num_progs=0,producer=""):
        self.name = name
        self.url = url
        self.img_url = img_url
        self.num_pages = 1
        self.num_progs = num_progs
        self.producer = producer

    def get_episodes(self,page=1):
        res = []
        
        if page < 1:
            page = 1
        elif page > self.num_pages:
            page = self.num_pages
        
        #Get page
        r = requests.get(self.url.replace("_1.html","_"+str(page)+".html"))
        
        if r.status_code != 200:
            print("Error al descargar la pagina:",r,file=sys.stderr)
            exit(-1) 
        
        #get programs  
        parser = AdvancedHTMLParser.AdvancedHTMLParser()
        parser.parseStr(r.content.decode(errors="replace"))        
        eps = parser.getElementsByClassName("modulo-type-episodio")
  
        #self.num_paginas = len(parser.getElementsByClassName("pagination")[0].children)
        
        #iterate episodes
        for ep in eps:
            name = ep[3][0][0].attributes["title"]
            url = ep[3][0][0].attributes["href"]
            duration = ep.getElementsByClassName("time")[0].innerText
            date = ep.getElementsByClassName("date")[0].innerText
            date_alt = ep.getElementsByClassName("date")[0].attributes["title"]

            res.append(episode(url,name,duration,date,date_alt))

        self.episodes = res

def search_podcast(name,page=1):
    res = []
    
    #get page
    name_busqueda = name.replace(" ","-")
    url_busqueda = "https://www.ivoox.com/"+name_busqueda+"_sw_1_"+str(page)+".html"
    
    r = requests.get(url_busqueda)
    '''if r.status_code != 200:
        print("Error al descargar la pagina:",r,file=sys.stderr)
        exit(-1)'''
    
    parser = AdvancedHTMLParser.AdvancedHTMLParser()
    parser.parseStr(r.content.decode(errors="ignore"))        
    
    #pages
    num_pags = 1
    try:
      num_pags = len(parser.getElementsByClassName("pagination").children)-2
    except:
      pass
    
    #list of podcast
    progs = parser.getElementsByClassName("modulo-type-programa")
    
    #iterate podcasts
    for prog in progs:
        #name
        name = prog.getElementsByAttr("itemprop","name")[0].attributes["content"]
        
        #url        
        url = prog.getElementsByAttr("itemprop","url")[0].attributes["content"]

        #imagen
        img = prog.getElementsByClassName("header-modulo")[0].children[0].children[0]
        img_url = img.attributes["data-src"]
        
        #num progs
        num = prog.getElementsByClassName("microphone")[0].children[0]
        num_progs = num.innerText

        #producer
        prod = prog.getElementsByClassName("wrapper")[0].children[2]
        prod_name = prod.attributes["title"]

        res.append(podcast(url,
          name,
          img_url,
          num_progs=num_progs,
          producer=prod_name))
    
    return res
