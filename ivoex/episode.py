import requests
import re

class episode:
    name = None
    url = None
    audio_link = None
    duration = None
    date = None
    date_alt = None
    
    def __init__(self,url,name = None,duration = 0,date="x",date_alt="x"):
        self.name = name
        self.url = url
        self.duration = duration
        self.date = date
        self.date_alt = date_alt
    
    def get_data(self):
        r = requests.get(self.url)
        if r.status_code != 200:
            print("Error al descargar la pagina:",r,file=sys.stderr)
            exit(-1)
        
        url_des = re.search("(downloadlink_.*)'",r.content.decode(errors="replace")).groups()[0]
        r_final = requests.get("https://www.ivoox.com/"+url_des)
        
        if r_final.status_code != 200:
            print("Error al descargar la pagina:",r,file=sys.stderr)
            exit(-1)
        
        url_audio = re.search("href=\"([a-zA-Z0-9:/_.?=-]*)\"",r_final.content.decode(errors="replace")).groups()[0]
        
        self.name = "todo"
        self.duration = "todo"
        self.audio_link = url_audio
