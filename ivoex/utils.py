import sys
import socket

def print_pb(size=10,value=0):
  cadena = "[" + "·"*(value) + " "*(size-value) + f"] {round(100*value/size,2)}%"

  sys.stdout.write("\b"*len(cadena))
  sys.stdout.write(cadena)
  sys.stdout.flush()

def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except Exception:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP
