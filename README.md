# ivoox-explorer

Programilla en python para buscar la url de los audios de iVoox. Proyecto no oficial de ivoox.

Licencia: GPLv3

# Uso

Recibe como primer argumento el nombre del podcast a buscar, posteriormente aparece un menu para seleccionar el podcasts y sus capitulos.

La interfaz se imprime en stderr, meintras que la url del audio se imprime en stdout, por tanto se puede usar una pipe :)
