import os
import io
import threading
from flask import Flask
from flask import render_template
from flask import request
from flask import Response
from flask import redirect
from flask import stream_with_context
from flask import send_from_directory
from flask import send_file
from waitress import serve

from mpd import MPDClient

import ivoex.utils as utils
from ivoex.episode import *
from ivoex.program import *
from ivoex.session import *
    
import random


app = Flask(__name__)
global_sessions = {}

mpd_config = {
    "mpd_host":"127.0.0.1",
    "proxy_ip":utils.get_ip()+":5000"
}

@app.route("/")
def root():
    return redirect("/search_program", code=302)

@app.route("/search_program")
def search_programs():
    search_name = request.args.get("name")
    current_session,_ = get_current_session(request,global_sessions)
    
    if search_name != None and search_name != "":
        if search_name != current_session.search_name:
            current_session.search_programs(search_name)
    
    #global_sessions[current_session.id] = current_session
    return render_template("search_program.html",
        sesion=current_session,
        programs_indexed=zip(
            current_session.podcast_list,
            range(len(current_session.podcast_list))
          )
        )
        

@app.route("/search_episodes")
def search_episodes():
    current_session,exists = get_current_session(request,global_sessions)
    pindex = int(request.args.get("pindex"))

    if not exists:
      return redirect("/search_program",code=302)
    
    if current_session.current_podcast != current_session.podcast_list[pindex]:
        current_session.current_podcast = current_session.podcast_list[pindex]
        current_session.search_episodes()    

    return render_template("search_episodes.html",
        sesion=current_session,
        episodes_indexed=zip(
            current_session.current_podcast.episodes,
            range(len(current_session.current_podcast.episodes))
          )
        )

@app.route("/play_episode")
def play_episode():
    current_session,_ = get_current_session(request,global_sessions)
    eindex = int(request.args.get("eindex"))
    
    current_session.current_episode = current_session.current_podcast.episodes[eindex]
    current_session.current_episode.get_data()

    return render_template("play_episode.html",sesion=current_session,mpd_config=mpd_config)

@app.route("/mpd_play")
def mpd_play():
    current_session,_ = get_current_session(request,global_sessions)
    client = MPDClient()

    try:
        client.connect(request.args.get('mpd_host'),6600)
          
    except:
        return "fallo conectando con el servidor"
        
    try:
        url = f"http://{request.args.get('proxy_ip')}/serve_episode?id={current_session.id}"
        
        if "clear" in request.args.keys():
          client.playlistclear()
        
        client.add(url)
          
        if "play" in request.args.keys():
          client.play()
        
    except:
        return "fallo añadiendo url"
        
    status = client.status().items()
        
    client.close()
    client.disconnect()
    
    return render_template("mpd_play.html",status=status)

@app.route("/serve_episode")
def serve_episode():
    current_session,_ = get_current_session(request,global_sessions)

    referer = current_session.current_episode.url
    headers = { "Referer":referer }
    req = requests.get(current_session.current_episode.audio_link,headers=headers,stream=True)
    total_size = int(req.headers["Content-Length"])

    chunk_size = 1024*128
    lock = threading.Lock()

    if current_session.current_buffer == None:
      print("")
      print(f"Iniciando nueva descarga... ({referer})")

      current_session.current_buffer = io.BytesIO()
      def request_audio():
        actual_size = 0
        for chunk in req.iter_content(chunk_size=chunk_size):
          lock.acquire()
          current_session.current_buffer.write(chunk)
          lock.release()

          actual_size = actual_size + len(chunk)       
          utils.print_pb(40,int(40*actual_size/total_size))

      threading.Thread(target=request_audio,daemon=True).start()

    @stream_with_context
    def download_audio():
      print("Sirviendo audio")

      xfered = 0
      while xfered <= total_size:
        if xfered + chunk_size < current_session.current_buffer.tell():
          lock.acquire()
          tmp = current_session.current_buffer.tell()
          current_session.current_buffer.seek(xfered)
          chunk = current_session.current_buffer.read(chunk_size)
          current_session.current_buffer.seek(tmp)
          lock.release()

          xfered = xfered + chunk_size
          yield chunk
      req.close()       

    return Response(download_audio(),mimetype="audio/mpeg")#,headers={"Accept-Ranges":"bytes"})
